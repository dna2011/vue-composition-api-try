import { ref } from '@vue/composition-api';

export const useToggle = (initial = false) => {
  const active = ref(initial);

  const toggle = () => {
    active.value = !active.value;
  };

  return {
    active,
    toggle,
  };
};
