import { reactive, computed } from '@vue/composition-api';

export const useRecipes = () => {
  const state = reactive({
    recipes: [],
    selectedID: null,
  });

  const selectedRecipe = computed(() =>
    state.recipes.find((r) => r.id === state.selectedID)
  );

  const addRecipe = (item) => {
    state.recipes.push(item);
  };

  const selectRecipe = (id) => {
    state.selectedID = id;
  };

  const removeRecipe = (id) => {
    state.recipes = state.recipes.filter((r) => r.id !== id);
  };

  return {
    state,
    selectedRecipe,
    addRecipe,
    selectRecipe,
    removeRecipe,
  };
};
