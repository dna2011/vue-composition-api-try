import { reactive, computed } from '@vue/composition-api';

export const useForm = (emit) => {
  const form = reactive({
    title: '',
    description: '',
    valid: computed(() => form.title.trim() && form.description.trim()),
  });

  const submit = () => {
    const recipe = {
      title: form.title.trim(),
      description: form.description.trim(),
      id: Date.now().toString(),
    };

    emit('add', recipe);

    form.title = form.description = '';
  };

  return {
    form,
    submit,
  };
};
